-- Bounnoy Phanthavong, CS 320 - Homework 7
module HW7 where
import Data.Map (Map)
import qualified Data.Map as S

-- Data abstractions.
data Expr
  = Var String
  | Val Integer
  | Bin Char Expr Expr
  deriving Show

data Stmt
  = Assign Expr Expr
  | While Expr Stmt
  | If Expr Stmt Stmt
  | Comp [Stmt]
  | Out Expr
  deriving Show

-- Pretty printing.
tab (n) = concat $ replicate n " "

pp (Var e) = e
pp (Val e) = show e
pp (Bin o e1 e2) = "(" ++ pp e1 ++ " " ++ [o] ++ " " ++ pp e2 ++ ")"

ppc n (x:xs) = if null xs then pps n x ++ "\n" else pps n x ++ "\n" ++ ppc n xs

pps n (Assign v e) = tab(n) ++ pp v ++ " := " ++ pp e
pps n (While e s) = tab(n) ++ "while " ++ pp e ++ " do\n" ++ pps (n+2) s
pps n (If e s1 s2) = tab(n) ++ "if " ++ pp e ++ " then\n" ++ pps (n+2) s1 ++ "\nelse\n" ++ pps (n+2) s2
pps n (Comp s) = tab(n) ++ "begin\n" ++ ppc (n+2) s ++ tab(n) ++ "end"
pps n (Out v) = tab(n) ++ "output " ++ pp v

-- Evaluating
type Sym = S.Map String Integer

eval :: Expr -> Sym -> Integer
eval (Var e) sym = if S.member e sym then sym S.! e else error "no key/value pair"
eval (Val e) _ = e
eval (Bin o e1 e2) sym
  | o == '+'  = eval e1 sym + eval e2 sym
  | o == '-'  = eval e1 sym - eval e2 sym
  | o == '*'  = eval e1 sym * eval e2 sym
  | o == '%'  = eval e1 sym `mod` eval e2 sym
  | o == '/' && (eval e1 sym == 0 || eval e2 sym == 0 || eval e1 sym `mod` eval e2 sym /= 0) = error "division error"
  | o == '/'  = eval e1 sym `div` eval e2 sym

check (Bin o e1 e2) sym
  | o == '<' && (eval e1 sym < eval e2 sym) = True
  | o == '>' && (eval e1 sym > eval e2 sym) = True
  | otherwise = False

exe :: Stmt -> Sym -> Sym
exe (Assign (Var v) e) sym = S.insert v (eval e sym) sym
exe (While e s) sym = if (check e sym) then exe (While e s) (exe s sym) else sym
exe (If e s1 s2) sym = if (check e sym) then exe (s1) sym else exe (s2) sym
exe (Comp (x:xs)) sym = if null xs then exe x sym else exe (Comp xs) (exe x sym)

out :: Stmt -> Sym -> String
out (Out e) sym = pp e ++ " = " ++ show (sym S.! (pp e))

-- Factorial and Least Common Multiple functions.
mapfactorial = exe (Comp [
  (Assign (Var "n") (Val 5)),
  (Assign (Var "r") (Val 1)),
  (While (Bin '>' (Var "n") (Val 0)) (Comp [
    (Assign (Var "r") (Bin '*' (Var "r") (Var "n"))),
    (Assign (Var "n") (Bin '-' (Var "n") (Val 1)))]))
  ]) S.empty
exfactorial = putStrLn(out (Out (Var "r")) mapfactorial)
ppfactorial = putStrLn(pps 0 (Comp [
  (Assign (Var "n") (Val 5)),
  (Assign (Var "r") (Val 1)),
  (While (Bin '>' (Var "n") (Val 0)) (Comp [
    (Assign (Var "r") (Bin '*' (Var "r") (Var "n"))),
    (Assign (Var "n") (Bin '-' (Var "n") (Val 1)))])),
  (Out (Var "r"))]))

maplcm = exe (Comp [
  (Assign (Var "a1") (Val 16)),
  (Assign (Var "b1") (Val 36)),
  (Assign (Var "a2") (Val 16)),
  (Assign (Var "b2") (Val 36)),
  (Assign (Var "r") (Val 0)),
  (While (Bin '>' (Var "b1") (Val 0)) (Comp [
    (Assign (Var "r") (Bin '%' (Var "a1") (Var "b1"))),
    (Assign (Var "a1") (Var "b1")),
    (Assign (Var "b1") (Var "r"))])),
  (Assign (Var "a2") (Bin '/' (Bin '*' (Var "a2") (Var "b2")) (Var "a1")))]) S.empty
exlcm = putStrLn(out (Out (Var "a2")) maplcm)
pplcm = putStrLn(pps 0 (Comp [
  (Assign (Var "a1") (Val 16)),
  (Assign (Var "b1") (Val 36)),
  (Assign (Var "a2") (Val 16)),
  (Assign (Var "b2") (Val 36)),
  (Assign (Var "r") (Val 0)),
  (While (Bin '>' (Var "b1") (Val 0)) (Comp [
    (Assign (Var "r") (Bin '%' (Var "a1") (Var "b1"))),
    (Assign (Var "a1") (Var "b1")),
    (Assign (Var "b1") (Var "r"))])),
  (Assign (Var "a2") (Bin '/' (Bin '*' (Var "a2") (Var "b2")) (Var "a1"))),
  (Out (Var "a2"))]))

main = do
  putStrLn("------------------------------")
  putStrLn("        Factorial(5)")
  putStrLn("------------------------------")
  ppfactorial
  putStrLn("---------- Results -----------")
  exfactorial
  putStrLn(show (S.toList (mapfactorial)))
  putStrLn("------------------------------")
  putStrLn(" Least Common Multiple (16,36)")
  putStrLn("------------------------------")
  putStrLn("LCM Formula: (a * b) / gcd(a, b)")
  putStrLn("a1 and b1 below calculates gcd")
  putStrLn("a2 and b2 below calculates lcm\n")
  pplcm
  putStrLn("---------- Results -----------")
  exlcm
  putStrLn(show (S.toList (maplcm)))
  putStrLn("------------------------------")
