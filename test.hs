-- Bounnoy Phanthavong, CS 320 - Homework 7
module TestHW7 where
import Test.HUnit
import HW7
import Data.Map (Map)
import qualified Data.Map as S

-- Testing execution of statements.
ex1 = exe (Assign (Var "n") (Val 5)) S.empty
test1 = TestCase (assertEqual "Testing execution of assign statement: assign(n,5)" 5 (ex1 S.! "n"))
mes1 = do
  putStr("\nTesting execution of assign statement: assign(n,5)\n")
  putStr("Expected: n = 5\nActual: " ++ (out (Out (Var "n")) ex1) ++ "\n")

sym1 = exe (Assign (Var "n") (Val 10)) S.empty
ex2 = exe (While (Bin '>' (Var "n") (Val 5)) (Assign (Var "n") (Bin '-' (Var "n") (Val 1)))) sym1
test2 = TestCase (assertEqual "Testing execution of while statement (before): while(n>5, assign(n, n-1))" 10 (sym1 S.! "n"))
test3 = TestCase (assertEqual "Testing execution of while statement (after): while(n>5, assign(n, n-1))" 5 (ex2 S.! "n"))
mes2 = do
  putStr("\nTesting execution of while statement: while(n>5, assign(n, n-1))\n")
  putStr("Expected (before): n = 10\nActual: " ++ (out (Out (Var "n")) sym1) ++ "\n")
  putStr("Expected (after): n = 5\nActual: " ++ (out (Out (Var "n")) ex2) ++ "\n")

sym2 = exe (Assign (Var "n") (Val 10)) S.empty
ex3 = exe (If (Bin '>' (Var "n") (Val 5)) (Assign (Var "r") (Val 100)) (Assign (Var "r") (Val 999))) sym2
ex4 = exe (If (Bin '<' (Var "n") (Val 5)) (Assign (Var "r") (Val 100)) (Assign (Var "r") (Val 999))) sym2
test4 = TestCase (assertEqual "Testing execution of if statement (before): if(n>5, assign(r, 100), assign(r, 999))" 10 (sym2 S.! "n"))
test5 = TestCase (assertEqual "Testing execution of if statement (n>5): if(n>5, assign(r, 100), assign(r, 999))" 100 (ex3 S.! "r"))
test6 = TestCase (assertEqual "Testing execution of if statement (n<5): if(n>5, assign(r, 100), assign(r, 999))" 999 (ex4 S.! "r"))
mes3 = do
  putStr("\nTesting execution of if statement: if(n>5, assign(r, 100), assign(r, 999))\n")
  putStr("Expected (before): n = 10\nActual: " ++ (out (Out (Var "n")) sym2) ++ "\n")
  putStr("Expected (n>5): r = 100\nActual: " ++ (out (Out (Var "r")) ex3) ++ "\n")
  putStr("Expected (n<5): r = 999\nActual: " ++ (out (Out (Var "r")) ex4) ++ "\n")

ex5 = exe (Comp [(Assign (Var "n") (Val 10)), (Assign (Var "r") (Val 1))]) S.empty
test7 = TestCase (assertEqual "Testing execution of compound statement: comp(assign(n, 10), assign(r, 1))" 10 (ex5 S.! "n"))
test8 = TestCase (assertEqual "Testing execution of compound statement: comp(assign(n, 10), assign(r, 1))" 1 (ex5 S.! "r"))
mes4 = do
  putStr("\nTesting execution of compound statement: comp(assign(n, 10), assign(r, 1))\n")
  putStr("Expected: n = 10\nActual: " ++ (out (Out (Var "n")) ex5) ++ "\n")
  putStr("Expected: r = 1\nActual: " ++ (out (Out (Var "r")) ex5) ++ "\n")

sym3 = exe (Assign (Var "n") (Val 10)) S.empty
ex6 = out (Out (Var "n")) sym3
test9 = TestCase (assertEqual "Testing execution of out statement: assign(n, 10), out(n)" "n = 10" ex6)
mes5 = do
  putStr("\nTesting execution of out statement: assign(n, 10), out(n)\n")
  putStr("Expected: n = 10\nActual: " ++ ex6 ++ "\n")

-- Testing pretty printing of statements.
pp1 = pps 0 (Assign (Var "n") (Val 5))
test10 = TestCase (assertEqual "Testing pretty printing of assign statement: pps(0,assign(n,5))" "n := 5" pp1)
mes6 = do
  putStr("\nTesting pretty printing of assign statement: pps(0,assign(n,5))\n")
  putStr("Expected: n := 5\nActual: " ++ pp1 ++ "\n")

pp2 = pps 0 (While (Bin '>' (Var "n") (Val 5)) (Assign (Var "n") (Bin '-' (Var "n") (Val 1))))
test11 = TestCase (assertEqual "Testing pretty printing of while statement: pps(0,while(n>5,assign(n,n-1)))" "while (n > 5) do\n  n := (n - 1)" pp2)
mes7 = do
  putStr("\nTesting pretty printing of while statement: pps(0,while(n>5,assign(n,n-1)))\n")
  putStr("Expected:\nwhile (n > 5) do\n  n := (n - 1)\nActual:\n" ++ pp2 ++ "\n")

pp3 = pps 0 (If (Bin '>' (Var "n") (Val 5)) (Assign (Var "n") (Bin '-' (Var "n") (Val 1))) (Assign (Var "r") (Var "n")))
test12 = TestCase (assertEqual "Testing pretty printing of if statement: pps(0,if(n>5,assign(n,n-1),assign(r,n)))" "if (n > 5) then\n  n := (n - 1)\nelse\n  r := n" pp3)
mes8 = do
  putStr("\nTesting pretty printing of if statement: pps(0,if(n>5,assign(n,n-1),assign(r,n)))\n")
  putStr("Expected:\nif (n > 5) then\n  n := (n - 1)\nelse\n  r := n\nActual:\n" ++ pp3 ++ "\n")

pp4 = pps 0 (Comp [(Assign (Var "n") (Val 1)), (Assign (Var "r") (Val 2)), (Assign (Var "s") (Val 5))])
test13 = TestCase (assertEqual "Testing pretty printing of compound statement: pps(0,comp([assign(n,1),assign(r,2),assign(s,5)]))" "begin\n  n := 1\n  r := 2\n  s := 5\nend" pp4)
mes9 = do
  putStr("\nTesting pretty printing of compound statement: pps(0,comp([assign(n,1),assign(r,2),assign(s,5)]))\n")
  putStr("Expected:\nbegin\n  n := 1\n  r := 2\n  s := 5\nend\nActual:\n" ++ pp4 ++ "\n")

pp5 = pps 0 (Out (Var "n"))
test14 = TestCase (assertEqual "Testing pretty printing of out statement: pps(0,out(n))" "output n" pp5)
mes10 = do
  putStr("\nTesting pretty printing of out statement: pps(0,out(n))\n")
  putStr("Expected: output n\nActual: " ++ pp5 ++ "\n")

-- Automate tests.
tests = TestList [
  TestLabel "test1" test1,
  TestLabel "test2" test2,
  TestLabel "test3" test3,
  TestLabel "test4" test4,
  TestLabel "test5" test5,
  TestLabel "test6" test6,
  TestLabel "test7" test7,
  TestLabel "test8" test8,
  TestLabel "test9" test9,
  TestLabel "test10" test10,
  TestLabel "test11" test11,
  TestLabel "test12" test12,
  TestLabel "test13" test13,
  TestLabel "test14" test14
  ]

testreport = do mes1; mes2; mes3; mes4; mes5; mes6; mes7; mes8; mes9; mes10
